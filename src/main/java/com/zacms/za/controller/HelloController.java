package com.zacms.za.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * hello
 */
@Controller
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping(value = "/say", method = RequestMethod.GET)
    @ResponseBody
    public Object say() {
        List<String> s = new ArrayList<>();
        s.add("hello");
        s.add(" my");
        s.add(" world!!");
        return s;
    }

}
