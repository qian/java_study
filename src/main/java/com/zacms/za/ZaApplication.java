package com.zacms.za;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZaApplication.class, args);
    }

}
